package com.demo.servicegato.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GatoController {

    @GetMapping
    public String getGato() {
        return "Hello gato";
    }
}
