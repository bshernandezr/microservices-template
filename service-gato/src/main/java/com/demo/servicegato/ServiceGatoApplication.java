package com.demo.servicegato;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ServiceGatoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceGatoApplication.class, args);
	}

}
