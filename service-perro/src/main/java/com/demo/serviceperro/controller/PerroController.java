package com.demo.serviceperro.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PerroController {

    @GetMapping
    public String getPerro() {
        return "Hello Perro";
    }
}
